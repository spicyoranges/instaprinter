class FullDbStructure < ActiveRecord::Migration
  def change
	  create_table "admins", :force => true do |t|
	    t.string   "login"
	    t.string   "password_digest"
	    t.datetime "created_at",      :null => false
	    t.datetime "updated_at",      :null => false
	  end

	  create_table "billings", :force => true do |t|
	    t.string   "orders"
	    t.text     "description"
	    t.decimal  "totalsum"
	    t.string   "billingtype"
	    t.string   "billing_status"
	    t.datetime "created_at",     :null => false
	    t.datetime "updated_at",     :null => false
	  end

	  create_table "orders", :force => true do |t|
	    t.integer  "user_id"
	    t.string   "status"
	    t.integer  "amount"
	    t.datetime "date"
	    t.string   "direction"
	    t.string   "region"
	    t.string   "city"
	    t.string   "comuna"
	    t.string   "postcode"
	    t.string   "phone"
	    t.string   "pdfref"
	    t.integer  "service_id"
	    t.string   "tracknum"
	    t.datetime "created_at",    :null => false
	    t.datetime "updated_at",    :null => false
	    t.string   "observations"
	    t.string   "delivery_type"
	  end

	  create_table "promocodes", :force => true do |t|
	    t.string   "code"
	    t.integer  "user_id"
	    t.boolean  "status"
	    t.datetime "activation"
	    t.datetime "created_at", :null => false
	    t.datetime "updated_at", :null => false
	    t.integer  "bonus"
	    t.datetime "use_until"
	    t.integer  "service_id"
	  end

	  create_table "regions", :force => true do |t|
	    t.string "region"
	    t.string "price_local"
	    t.string "price_ems"
	    t.string "price_dhl"
	  end

	  create_table "services", :force => true do |t|
	    t.string   "sname"
	    t.integer  "price"
	    t.string   "descr"
	    t.boolean  "status"
	    t.datetime "created_at",      :null => false
	    t.datetime "updated_at",      :null => false
	    t.text     "fulldescription"
	    t.text     "instruction"
	  end

	  create_table "users", :force => true do |t|
	    t.string   "login"
	    t.string   "fname"
	    t.string   "email"
	    t.string   "direction"
	    t.string   "comuna"
	    t.string   "city"
	    t.string   "region"
	    t.string   "postcode"
	    t.string   "phone"
	    t.string   "facelogin"
	    t.string   "twitlogin"
	    t.datetime "recent"
	    t.datetime "created_at", :null => false
	    t.datetime "updated_at", :null => false
	  end

	  create_table "admins", :force => true do |t|
	    t.string   "login"
	    t.string   "password_digest"
	    t.datetime "created_at",      :null => false
	    t.datetime "updated_at",      :null => false
	  end

	  create_table "billings", :force => true do |t|
	    t.string   "orders"
	    t.text     "description"
	    t.decimal  "totalsum",       :precision => 65, :scale => 0
	    t.string   "billingtype"
	    t.string   "billing_status"
	    t.datetime "created_at",                                    :null => false
	    t.datetime "updated_at",                                    :null => false
	  end

	  create_table "orders", :force => true do |t|
	    t.integer  "user_id"
	    t.string   "status"
	    t.integer  "amount"
	    t.datetime "date"
	    t.datetime "created_at",                          :null => false
	    t.datetime "updated_at",                          :null => false
	    t.string   "direction"
	    t.string   "region"
	    t.string   "city"
	    t.string   "comuna"
	    t.string   "postcode"
	    t.string   "phone"
	    t.string   "pdfref"
	    t.integer  "service_id"
	    t.text     "observations",  :limit => 2147483647
	    t.text     "tracknum",      :limit => 2147483647
	    t.string   "delivery_type"
	  end

	  create_table "promocodes", :force => true do |t|
	    t.string   "code"
	    t.integer  "user_id"
	    t.string   "status",     :limit => 5
	    t.datetime "activation"
	    t.datetime "created_at",              :null => false
	    t.datetime "updated_at",              :null => false
	    t.integer  "bonus"
	    t.datetime "use_until"
	    t.integer  "service_id"
	  end

	  create_table "regions", :force => true do |t|
	    t.string "region"
	    t.string "price_local"
	    t.string "price_ems"
	    t.string "price_dhl"
	  end

	  create_table "services", :force => true do |t|
	    t.string   "sname"
	    t.integer  "price"
	    t.string   "descr"
	    t.string   "status",          :limit => 5
	    t.datetime "created_at",                            :null => false
	    t.datetime "updated_at",                            :null => false
	    t.text     "fulldescription", :limit => 2147483647
	    t.text     "instruction",     :limit => 2147483647
	  end

	  create_table "users", :force => true do |t|
	    t.string   "login"
	    t.string   "fname"
	    t.string   "email"
	    t.string   "direction"
	    t.string   "comuna"
	    t.string   "city"
	    t.string   "region"
	    t.string   "postcode"
	    t.string   "phone"
	    t.string   "facelogin"
	    t.string   "twitlogin"
	    t.datetime "recent"
	    t.datetime "created_at", :null => false
	    t.datetime "updated_at", :null => false
	  end
  end
end
