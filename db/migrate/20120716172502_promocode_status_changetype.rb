class PromocodeStatusChangetype < ActiveRecord::Migration
  def change
    change_column :promocodes, :status, :string
  end
end
