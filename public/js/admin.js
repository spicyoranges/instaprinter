function adminformdo() {
	$.ajax({
		type: 'POST',
		url: '/conds',
		data: $('#adminform').serialize(),
		dataType: 'html',
		success: function(data){
			//alert(data);
			$('#tabledata').html(data).fadeIn();
		},
		error: function(data){
			alert('Error');
		},
	})
};

function trackformdo(id) {
	$.ajax({
		type: 'POST',
		url: '/track',
		data: $(id).serialize(),
		dataType: 'html',
		success: $('#adminform').submit()
	})
};

function tracksubmit(event, id) {
	if(event.keyCode==13){
		$(id).submit();
	}
};

function chtrack(id) {
	$('#trackfield'+id).attr('disabled', false);
	$('#sb'+id).attr('disabled', false);
	$('#trackfield'+id).focus();
}	

function collectOrders(id) {
	var target = $('#del'+id);

	if ( $('#'+id).is(':checked') ) {
    	target.val(id);
  } else {
    	target.val('');
  }

}

function delOrders() {
	$.ajax({
		type: 'POST',
		url: '/delord',
		data: $('#delform').serialize(),
		dataType: 'html',
		success: $('#adminform').submit()
	})
}

function subdel() {
	$('#delform').submit();
}

function selectAllOrders() {
	var checkbox = $(':checkbox');
	var chall = $('#chall');

	if ($('#chlink').text() == "Check all")
		{checkbox.attr("checked", true);
		 $('#chlink').text('Uncheck all');}
	else 
		{checkbox.attr("checked", false);
		 $('#chlink').text('Check all');}
}

function deletePromo(id) 
{	
	var promo = $('#'+id);
	$.ajax(
	{
		type: 'DELETE',
		url: '/adminka/promocodes/'+id,
		dataType: 'html',
		success: function()
		{
			promo.remove();
		}
	})
}
