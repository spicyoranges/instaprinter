// This adds placeholder support to browsers that wouldn't otherwise support it. 
$(function() {
   if(!$.support.placeholder) { 
      var active = document.activeElement;
      $(':text').focus(function () {
         if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
            $(this).val('').removeClass('hasPlaceholder');
         }
      }).blur(function () {
         if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
            $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
         }
      });
      $(':text').blur();
      $(active).focus();
      $('form:eq(0)').submit(function () {
         $(':text.hasPlaceholder').val('');
      });
   }
});

// Change article active class on mouse over
function css3box() {
	$("article.product").mouseover(function(){
	    $(this).removeClass("box1").addClass("box2").animate("slow");
	    }).mouseout(function(){
	    $(this).removeClass("box2").addClass("box1").animate("slow");
	});	
}

// Bind clicks to images in imputs and maintain selectors
function imageSelector() {	
	$('form#instagramstream img').bind('click', function(e){
		var a = e.target.id;
		var checkbox = $('input#photo_'+a);

		if ( $(this).hasClass('selected') ) {
			$(this).animate({opacity: '1'}, 500);
			$(this).removeClass('selected');
			$(this).parent('.imagebox').find('span').animate({opacity: '0.3'}, 500);
		} else {
			$(this).animate({opacity: '0.4'}, 500);
			$(this).addClass('selected');
			$(this).parent('.imagebox').find('span').animate({opacity: '1'}, 500);
		}

		if (checkbox.is(':checked')) {
			checkbox.attr('checked', false);
			checkbox.val();
		} else {
			checkbox.attr('checked', true);
			checkbox.val(a);
		}
	});

	return false;
}


// Fix scrolling
// https://github.com/bigspotteddog/ScrollToFixed#readme
function scrollToFixed() {
  $('header h1').scrollToFixed({
		marginTop: 0,
		preFixed: function() { $(this).fadeOut(); },
		postFixed: function() { $(this).fadeIn(); },		
		zIndex: 899
	});	
  $('.generic').scrollToFixed({
		marginTop: 100,
		preFixed: function() { $(this).fadeOut(); },
		postFixed: function() { $(this).fadeIn(); },		
		zIndex: 898
	});
  $('#sidebar').scrollToFixed({
			marginTop: 150,
      zIndex: 897
  });
} 

function hover_links(selector) {
   $(selector).hover(function() {
     $(this).stop().animate({opacity: "0.5"}, 'slow');
   },
   function() {
     $(this).stop().animate({opacity: "1"}, 'slow');
   });		
}
// blinker
function blink(what) {
	$(what).fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
}

function select_toogle() {
	$('a#selectoogle').toggle(
		function() {
			$(this).text("Cancelar selección");
			$(this).removeClass("selectall").fadeOut().addClass('deselectall').fadeIn();
			$("div#checkboxes input:not(:checked)").attr('checked', true);
			$("form#instagramstream img").animate({opacity: '0.4'}, 500);
			$("form#instagramstream img").addClass('selected');	
			$('input#submit').removeClass("disabled").attr("disabled", false);
			$('input#submit').addClass("large").animate({opacity: '1'}, 500);
			$('.imagebox span').animate({opacity: '1'}, 500);		
		},
		function() {
			$(this).text("Seleccionar todo");
			$(this).removeClass("deselectall").fadeOut().addClass('selectall').fadeIn();
			$("div#checkboxes input:checkbox:checked").attr('checked', false);
			$("form#instagramstream img").animate({opacity: '1'}, 500);
			$("form#instagramstream img").removeClass('selected');
			$('input#submit').removeClass("large").attr("disabled", true);
			$('input#submit').addClass("disabled").animate({opacity: '0.5'}, 500);
			$('.imagebox span').animate({opacity: '0.3'}, 500);				
		}
	);	
}

function validate_imageboxes(max) {
	$("form#instagramstream").on("click", "img", function(){
		var countchecked = $("form#instagramstream input:checkbox:checked").length;
		var less = max - 1;
		
		$("p.mycounter").html('Haz selecionado ' + countchecked + (countchecked <= 1 ? ' foto' : ' fotos'));
		
		if (countchecked >= 1) {
			$('input#submit').removeClass("disabled").attr("disabled", false);
			$('input#submit').addClass("large").animate({opacity: '1'}, 500);
		} 
		if (countchecked == 0) {
			$('input#submit').removeClass("large").attr("disabled", true);
			$('input#submit').addClass("disabled").animate({opacity: '0.5'}, 500);
		}		
		
		if (countchecked == max) {
		 	$("form#instagramstream input:not(:checked)").attr("disaled", "disaled");
			$("form#instagramstream img").not(".selected").offtmp("click");
			$("p.mycounter").html('Haz selecionado cantidad máxima de fotos. Proceda al paso siguiente.');
		}
		
		if (countchecked <= less) {
		 	$("form#instagramstream input:not(:checked)").removeAttr("disaled");
			$("form#instagramstream img").not(".selected").ontmp("click");	
		}
		
		blink("p.mycounter");
	});	
}

function validate_orderform() {
	// http://bassistance.de/jquery-plugins/jquery-plugin-validation/
	$('#orderstream').validate({
		debug: true,	
		onfocusout: false,
    invalidHandler: function(form, validator) {
        var errors = validator.numberOfInvalids();
        if (errors) {										
            //alert(validator.errorList[0].message);
						$('#ErrorHandler').html(validator.errorList[0].message);
						$('#ErrorHandler').animate({opacity: '0.8'}, 500);
						$('#ErrorHandler').slideDown("slow").delay(2000).slideUp("slow");
            validator.errorList[0].element.focus();
        }
    },
		rules: {
			fname : 'required',
			dir   : 'required',
			comuna: 'required',
			city  : 'required',
			region: 'required',
			email : { required: true, email: true },
			tel   : 'required',
			quantity: { required: true, number: true }
		},
		messages: {
			fname : 'Ingresa sus nombres y apellidos',
			dir   : 'Ingresa la dirección completa',
			comuna: 'Ingresa la comuna',
			city  : 'Ingresa la ciudad',
			region: 'Selecciona la region',
			email : { required: 'Campo email es obligatorio.', email: 'Formato del email ingresado es incorrecto' },
			tel   : 'Campo telefono es obligatorio',
			quantity: 'Opción de compra es invalida. Ingresa cantidad de copias del producto utilizando solamente caractéres numéricos'
		},
		errorPlacement: function(error, element) {
			// Override error placement to not show error messages beside elements //
		},
		submitHandler: function(form) { 
			$.fancybox(
				'#submitHandler',
				{
					'autoDimensions'	: false,
					'width'         	: 350,
					'height'        	: 'auto',
					'transitionIn'		: 'none',
					'transitionOut'		: 'none',
					'hideOnOverlayClick': false,
					'hideOnContentClick': false,
					'showCloseButton': false
				});
			form.submit();
		}
	});	
}

function check_promocode() {
	$("input#promocode").change(function(){	
		$.ajax({
			type: "POST",
			dataType: "html",
			data: { promocode: $(this).val() },
			url: "/check/promocode"	
		}).done(function(data){
			$('input#quantity').val("1");
			$('input#quantity').removeAttr("disabled");
			if (data == "1") {
				$('.promocodeStatus').html("El código ingresado ya fue utilizado");
			}
			else if (data == "2") {
				$('.promocodeStatus').html("El código ingresado no es valido para producto seleccionado");
			}
			else if (data == "3") {
				$('.promocodeStatus').html("El código ingresado es incorrecto o ya fue utilizado");
			}
			else {
				$('.promocodeStatus').html("Su descuento es "+data+"% off del costo de producto solicitado");
				$("#pcid_valid").val("1");
				$('input#quantity').attr("disabled", true);
			}
		});		
	});
}