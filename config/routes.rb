Instaprint::Application.routes.draw do

  resources :pages
  resources :feed
  resources :services
  resources :orders
  
  get "help/about"
  get "help/payment"
  get "help/politics"
  get "help/faq"  
	
	# Main index page
  root :to => "Pages#index"
  
  # Connection routes
  match "/connect" , :to => "sessions#connect"
  match "/callback" , :to => "sessions#callback"
  match "/logout" , :to => "sessions#logout"
  
  # Services & products
  match "/services" , :to => "services#index"
  match "/service" , :to => "services#show"
  
  match "/profile" , :to => "feed#profile"
  match "/instafeed" , :to => "feed#index"
  match "/construct" , :to => "constructor#show"
  
  # Orders
  match "/book" , :to => "orders#new"
  match "/book" , :to => "orders#new", :via => :post
  match "/paymethod", :to => "orders#paymethod"
  match "/check/promocode", :to => "promocodes#validate"

  # Payment actions
  match "/payment", :to => "orders#payment"
  
  match "/payment/pagomaster", :to => "payment#pagomaster", :via => :post
  match "/pagomaster/success", :to => "orders#payment"
  match "/pagomaster/fail", :to => "orders#paymenterror"
    
  match "/payment/robox", :to => "payment#robox", :via => :post
  match "/robokassa/success", :to => "orders#payment"
  match "/robokassa/fail", :to => "orders#paymenterror"
      
  # PDF generation
  match "/pdfgen", :to => "orders#pdfgen"

  # Shopping cart
  match "/basket", :to => "orders#paymethod"
  match "/delete", :to => "basket#delete"
  match "/update", :to => "basket#update"

  # Help section
  match "/help/:action", :to => "help#:action"
  
  # Admin routes
  match "/adminka", :to => "admin#index"
  match "/check", :to => "admin#check"
  match "/adlogin", :to => "admin#login"
  match "/adlogout", :to => "admin#logout"
  match "/conds", :to => "admin#conditions"
  match "/track", :to => "admin#track"
  match "/delord", :to => "admin#delete"
  match "/showadmins", :to => "admin#showadmins"
  match "/editadmin", :to => "admin#editadmin"
  match "/createadmin", :to => "admin#createadmin"

  # Promocodes 
  resources :promocodes, :path => "/adminka/promocodes"

  #help
  match "/help/:action", :to => "help#:action"
  match "/contacts", :to => "pages#contacts"

end