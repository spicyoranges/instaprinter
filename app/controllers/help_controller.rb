# -*- coding: utf-8 -*-
class HelpController < ApplicationController
  def about
  end

  def payment
  end

  def politics
  end

  def faq
  end
end
