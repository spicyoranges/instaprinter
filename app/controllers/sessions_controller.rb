# -*- coding: utf-8 -*-
class SessionsController < ApplicationController
  def connect # соединение с инстаграм (сердце этой темы)
  	
  	@title = "Connection ..." # Заголовок страницы
  	
    # Переадресация домой если не выбран сервис и к выбору фоток в противном случае   
    if params[:s] 
    	session[:s] = params[:s]  # Запись сервиса в сессию если он есть
    end
        
    #redirect to auth of instagram and callback to this url
	  redirect_to Instagram.authorize_url(:redirect_uri => CALLBACK_URL)
  	  
  end

  def callback # Получение кода доступа
  	
  	@title = "Connection ..." # заголовок страницы
  	
  	#getting access token and callback and redirect to Feed index
  	response = Instagram.get_access_token(params[:code], :redirect_uri => CALLBACK_URL)
    session[:access_token] = response.access_token # Запись кода в сессию
    
    # Запись в сессию аватара и логина пользователя
    client = Instagram.client(:access_token => session[:access_token])
    @user = client.user
    #session[:avatar] = @user.profile_picture
    session[:usr] = @user.username
    
    # выборка кол-ва заказов для корзины
    session[:scsum] = Order.where('status = ? AND user_id = ?', "Generado", User.find_by_login(session[:usr])).count
     
    # Переадресация домой если не выбран сервис и к выбору фоток в противном случае   
    if !session[:s] 
    	redirect_to root_path 
    else 
      #session[:s] = params[:s]  # Запись сервиса в сессию если он есть
    	redirect_to url_for(:controller => 'feed', :action => 'index')
    end
  	
  end
  
  def logout # Завершение сессии
  	
  	if !session[:access_token]
  		redirect_to root_path
  	else    
      reset_session
      redirect_to root_path
    end
    
  end
end
