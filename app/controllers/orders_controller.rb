# -*- coding: utf-8 -*-
class OrdersController < ApplicationController
  def new #создание формы регистрации заказа
  	
  	@title = "Completar los datos de envio  | Instaprinter - Impresión de fotos Instagram"
  	
  	#Проверка корректности перехода на страницу !
  	if !params[:action_hook]
  		if !session[:s]
  			redirect_to root_path 
  		else
  			redirect_to :controller => "feed", :action => "index", :s => session[:s]
  		end
  	end
  	
  	@serv = Service.find(session[:s])
  	
  	#Генерация бредкрамбсов
  	add_breadcrumb "Completar formulario" , book_path, nil, 'bread'
  	
  	#Инициализация массива данных пользователя
  	@userdata = {:fname => '', :email => '', :dir => '', :comuna => '', :city => '', :region => '', :postcode => '', :phone => '', :face => '', :twit => '', :indb => false}
  	
    #Присвоение логина переменной    
    @user = session[:usr]
    
    #Если пользователь в базе заполнение данных в хеши
    if User.find_by_login(@user)
    	u = User.find_by_login(@user)
    	@userdata[:fname] = u.fname
    	@userdata[:email] = u.email
    	@userdata[:dir] = u.direction
    	@userdata[:comuna] = u.comuna
    	@userdata[:city] = u.city
    	@userdata[:region] = u.region
    	@userdata[:postcode] = u.postcode
    	@userdata[:phone] = u.phone
    	@userdata[:face] = u.facelogin
    	@userdata[:twit] = u.twitlogin
	  end

    # Should be in a model, but we are angry mad people
    # and using wrong methods, anyway
    # just let's go with in

    # Этот метод тащит все регионы для формы
    @regions = Regions.find(:all, :order => "id")
    @selected_region = @userdata[:region]
  
  end

  def create #обработка формы заказа
  	
  	@title ="Creating order" #Заголовок страницы
  	
  	#Переадресация если нет ключа
  	redirect_to :controller => 'sessions', :action => 'connect' if !session[:access_token]
  	
  	#Проверка корректности входа на страницу
  	redirect_to root_path if !params[:email]
  	
  	if !User.find_by_login(session[:usr]) #если юзера нет в базе
  		#Создание нового пользователя
 		@user = User.new(:login => session[:usr], :fname => params[:fname], :email => params[:email], :direction => params[:dir], 
 		:comuna => params[:comuna], :city => params[:city], :region => params[:region], :postcode => params[:cp], :phone => params[:tel], :facelogin => params[:face], :twitlogin => params[:twit])
   		if @user.save! #Если сохранение удачно => создание папки для отчетов
  			#Dir.mkdir(Rails.root.join("reports")) if Order.count(:all) == 0
   			Dir.mkdir(Rails.root.join("reports/#{session[:usr]}"))
   		end
   	else #Если есть в базе обновление данных
   		User.update(User.find_by_login(session[:usr]).id, :fname => params[:fname], :email => params[:email], :direction => params[:dir], :comuna => params[:comuna], :city => params[:city], :region => params[:region], :postcode => params[:cp], :phone => params[:tel], :facelogin => params[:face], :twitlogin => params[:twit])
   	end	
 	
    # внести данные в поле "замечания (observations)"
    if !params[:tshirtsize]
      observations = ""
    else
      observations = "Talla de polera: #{params[:tshirtsize]} | Color de polera: #{params[:tshirtcolor]}"
    end 	
    
    order_amount = params[:quantity]
    order_amount = "1" if !params[:pcid_valid].nil?    

 	  # создание нового заказа
 	  @order = Order.new(:user_id => User.find_by_login(session[:usr]).id, :amount => order_amount, :service_id => session[:s], :status => "Recibido", :date => Time.now, :direction => params[:dir], :comuna => params[:comuna], :city => params[:city], :region => params[:region], :postcode => params[:cp], :phone => params[:tel])
 		
 	  #сохранение заказа и переадресация на генерацию файла
 	  if @order.save!
      # Promocode line (July update)
      session[:free] = nil
      session[:promocode] = nil
      if !params[:promocode].blank?
        if !Promocode.find_by_code(params[:promocode]).nil? && params[:pcid_valid] == "1"
          session[:promocode] = Promocode.find_by_code(params[:promocode]).bonus
          pcid = Promocode.find_by_code(params[:promocode]).id
          # обновить статус промокода, если таковой выбран и действителен
          Promocode.update(pcid, :status => "IN_USE", :user_id => User.find_by_login(session[:usr]).id, :activation => Time.now.to_formatted_s(:db),:order_id => @order.id) if !session[:promocode].nil?    
          
          # все ок. logger.debug "\n\n\ время активации: #{Time.now.to_formatted_s(:db)}\n\n"      
        end
        session[:free] = true if session[:promocode] == 100
        # logger.debug "\n\n\ promocode bonus : #{session[:promocode]}\n\n"
        # logger.debug "\n\n\ session true/false : #{session[:free]}\n\n"
    	end 	    
 	    
 		 session[:o] = User.find_by_login(session[:usr]).orders.last.id 
     session[:promocode] = pcid
    redirect_to pdfgen_path :photos => params[:photos], :posterbg => params[:posterbg], :posterpos => params[:posterpos]
 	  end 

  end
  
  def pdfgen #Генерация файла заказа
  	
  	#Переадресация на выбор фоток если нет их
  	if !params[:photos] 
      redirect_to url_for(:controller => 'feed', :action => 'index') 
    else
  	  @title ="Creating order ..." #Заголовок страницы
  	
      if params[:posterbg]
 	      @color = params[:posterbg] # Принимаем цвет фона
      else
        @color = nil
      end
 		
 	    @photos = [] # Создаем массив ссылок на фотки
 	    i = 0
	    params[:photos].each do |ph|
		    @photos[i] = Instagram.client(:access_token => session[:access_token]).media_item(ph).images.standard_resolution.url
		    i += 1
	    end

        if params[:posterpos] == "false"
          @isVertical = false # 0 = false, horizontal
        else
          @isVertical = true # 1 = true, Wether poster is vertical or horizontal   
        end
 	      
        #procedure of generation of pdf
        output = OrderReport.new(:skip_page_creation => true).to_pdf session[:o], @photos, @color, @isVertical # Магниты
        
        if session[:free] == true
          s = []
          s << session[:o]
          session[:o] = s
        end
                  
        redirect_to basket_path
    end
  	
  end

  def paymethod

    @title ="Carro de compras  | Instaprinter - Impresión de fotos Instagram" # Заголовок страницы
    add_breadcrumb "Carro de compras" , basket_path, nil, 'bread'

    #Переадресация если нет ключа
    if !session[:access_token] 
      redirect_to root_path 
    else
      @basket = Order.where('status = ? AND user_id = ?', "Generado", User.find_by_login(session[:usr])).order(:direction)
      @prodset = @basket.ids
      # @mas = Array.new
      # prodset.each do |c|
      #   @mas << @basket.find_by_direction(c)
      # end
      
      i = 0
      @mas = []
      @prodset.each do |c|
        bas = Order.where('status = ? AND user_id = ? AND direction = ?', "Generado", User.find_by_login(session[:usr]), c)
        m = []
        
        bas.each do |b|
          m << b
          @mas[i] = m
        end
        
        i += 1
      end

      # странные вещи творятся здесь
    	@only_products = Array.new
    	@basket.each do |pr| 
    	  @only_products << pr.id 
    	end
    	session[:o] = @only_products	
    
      if session[:billing]
        # если есть сессия с биллингом, и это новый заход в корзину
        # то нужно обновить данные в БД
        @billling = Billing.update(session[:billing][:bid], :orders => "#{session[:o]}", :description => "Order updated. Still in shoping cart")
        
        @billingdata = session[:billing]
        @billingdata[:orders] = session[:o]
        @billingdata[:description] = "Order updated. Still in shoping cart"
      else
        # Если это первый заход, то нужно создать хеш
        # Тут начинаются извращения для робокассы
        @billing = Billing.new(:orders => "#{session[:o]}", :description => "Creating new order", :totalsum => "", :billingtype => "", :billing_status => "PREPARETOBILL")
        @billing.save
      
        @billingdata = Hash.new   
        @billingdata[:bid]            = "#{@billing.id}"
        @billingdata[:orders]         = "#{@billing.orders}"
        @billingdata[:description]    = "#{@billing.description}"
        @billingdata[:totalsum]       = "#{@billing.totalsum}"
        @billingdata[:billingtype]    = "#{@billing.billingtype}"
        @billingdata[:billing_status] = "#{@billing.billing_status}"
        @billingdata[:usermail]       = "#{User.find_by_login(session[:usr]).email}"
      end
      session[:billing] = @billingdata # save to session all billing data
      
      # обновить выборку кол-ва заказов для корзины
      session[:scsum] = @only_products.count
      
      
      redirect_to payment_path if session[:free] == true
    end
  end
  
  def payment # Оплата или спасибо
  	
  	@title ="Gracias por compra :: InstaPrinter" # Заголовок страницы
  	
  	#Переадресация если нет ключа
  	if !session[:access_token] 
      redirect_to root_path 
  	  #Если нет заказа
  	elsif !session[:o]
      redirect_to :controller => 'orders', :action => 'new'
  	else 
      #logger.debug "Session[:billing] = #{session[:billing]}"
      billed_orders = Billing.find(session[:billing][:bid]).orders
      psid = billed_orders[1..billed_orders.size-2].split(",").each {|t| t.strip!}

      psid.each do |s|
         Order.update(s, :status => "Pagado")
      end
      
      # обновить статус промокода 
      Promocode.update(session[:promocode], :status => "USED") if !session[:promocode].nil?
        
      # Billing order status update
      if session[:billing]
        Billing.update(session[:billing][:bid], :description => "Order processed", :billing_status => 'PROCESSED_OK') 
      end
      
  	  # Sending email to user
  	  o = session[:billing][:bid]
      UserMailer.confirmail(o,session[:usr]).deliver

      # обновить выборку кол-ва заказов для корзины
      session[:scsum] = Order.where('status = ? AND user_id = ?', "Generado", User.find_by_login(session[:usr])).count
      
      # clear sessions with orders and billing
      session[:o] = nil
      session[:billing] = nil
      session[:free] = nil
      session[:promocode] = nil
    end

  end

  def paymenterror
  end
  
end
