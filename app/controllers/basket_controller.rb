# -*- coding: utf-8 -*-
class BasketController < ApplicationController
  def index
    if !session[:access_token]
      redirect_to :controller => "pages", :action => "index"
    else
      @basket = Order.where('status = ? AND user_id = ?', "Generado", User.find_by_login(session[:usr]))
    end
  end
  
  def delete
    if request.xhr?
      @del_ord = Order.find(params[:oid])
      
      if @del_ord  
        File.delete("#{Rails.root}/reports/#{Order.find(params[:oid]).user.login}/#{Order.find(params[:oid]).date.strftime('%Y%m%d')}_#{params[:oid]}.pdf")
        @del_ord.destroy

        # обновить выборку кол-ва заказов для корзины
        session[:scsum] = Order.where('status = ? AND user_id = ?', "Generado", User.find_by_login(session[:usr])).count
        
        # обновить статус прокода, если он был использован для этого заказа
        pcid = Promocode.find_by_order_id(params[:oid])
        Promocode.update(pcid.id, :status => "FREE", :order_id => "") if !pcid.nil? && pcid.status == "IN_USE"
      end

      respond_to do |format|
        format.js
      end
    else
      redirect_to :controller => "pages", :action => "index"
    end
  end
  
  def update
    #Переадресация если нет ключа
    if !session[:access_token] 
      redirect_to root_path 
    else
    	@basket_result = params[:hueta]

    	@psid = Array.new # psid - products id for use in Payment Successful
      @basket_result.each do |i, d|
        Order.update(d[0], :delivery_type => d[1])
        @psid << d[0]
      end
    	session[:o] = @psid # write to session   
    end    
  end
end