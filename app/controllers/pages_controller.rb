# -*- coding: utf-8 -*-
class PagesController < ApplicationController
  def index #Главная
  	
  	@title = "Instaprinter - Impresión de fotos Instagram. Hacemos magnetos, stickers, poster con tus fotos de Instagram solamente en 3 pasos" # Заголовок страницы
    @dbproducts = Service.order("id") # Выбор из базы всех сервисов
    
    # Освобождение сессии от заказа и сервиса и бредов
    session[:o] = nil
    session[:s] = nil
    session[:bread] = nil
       
    add_breadcrumb "Principal" , root_path, nil, 'bread' # Генерация бредкрамбсов
    
  end

  def contacts
    @title = "Contactos :: InstaPrinter"
    add_breadcrumb "Contactos", contacts_path, 1, 'bread' 
  end

end
