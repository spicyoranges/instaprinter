# -*- coding: utf-8 -*-
require 'net/http'
require 'uri'

class PaymentController < ApplicationController  
  def robox
    if !session[:access_token] 
      redirect_to root_path 
    else        
      bid         = params[:robox_bid]
      orders      = params[:robox_orders]
      amount      = params[:robox_amount]
      description = "Payment has been sent to Robox"
      usermail    = params[:robox_umail]
      payment_type = "ROBOX"
          
      # Update billing
      Billing.update(bid, :orders => orders, :description => description, :totalsum => amount, :billingtype => payment_type, :billing_status => 'SENT_TO_ROBOX')
      session[:billing] = bid
    
      pay_url = Robokassa.client.init_payment_url(bid, amount, description, '', 'ru', usermail, {})
      redirect_to "#{pay_url}"
    end
  end
  
  def pagomaster
    if !session[:access_token] 
      redirect_to root_path 
    else  
      # common vars      
      bid         = params[:merchant_transaction_id]
      orders      = params[:pmaster_orders]
      amount      = params[:amount]
      usermail    = params[:emailbuyer]
      payment_type = "PAGOMASTER"      
      description = "Payment has been sent to #{payment_type}"
          
      # Update billing
      Billing.update(bid, :orders => orders, :description => description, :totalsum => amount, :billingtype => payment_type, :billing_status => 'SENT_TO_PAGOMASTER')
      session[:billing] = bid
    end

  end  
  
  def paypal
  end
end