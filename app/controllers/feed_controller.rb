# -*- coding: utf-8 -*-
class FeedController < ApplicationController
  def index
    
    @title = "Seleccionar fotos | Instaprinter - Impresión de fotos Instagram" #Тайтл страницы
    
    # Переадресация домой если не выбран сервис и к выбору фоток в противном случае   
    if params[:s] 
      session[:s] = params[:s]  # Запись сервиса в сессию если он есть
    end
        
    #Переадресация если нет ключа
    if (!session[:access_token])and(!session[:s])
       redirect_to root_path
    elsif (!session[:access_token])and(session[:s])
      redirect_to :controller => 'sessions', :action => 'connect'
    else

    #Подключение клиента и выбор данных
    count_per_query = 60
    client = Instagram.client(:access_token => session[:access_token])
    @user = client.user
    user_ids = Instagram.user_search(@user)
    user_id = user_ids.first
    first_query = client.user_recent_media({count: count_per_query})
    max_id = first_query.last.id
   
    
    i = 0
    ret = []
    first_query.each do |fq|
      ret << fq   
    end
    while true do 
      query = client.user_recent_media(user_id, {access_token: session[:access_token], count: count_per_query, max_id: max_id})
      break if query.empty? 
      query.each do |q|
        ret << q  
      end
      max_id = query.last.id
      i += 1
    end

    @recent_media_items = ret
    
    @serv = Service.find(session[:s])
  
    #Генерация бредкрамбсов
    add_breadcrumb "Seleccionar Fotos" , instafeed_path, nil, 'bread'
    
    end
    
  end

  def profile # Unused
    
    redirect_to root_path
        
  end
end