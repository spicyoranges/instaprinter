class PromocodesController < ApplicationController

#==============================Index Page ======================================

  def index
  	@title = "Promocodes::Instaprinter" #setup title
  	add_breadcrumb "Promocodes", promocodes_path, 1, 'abread' # add breadcrumbs
  	@Promo = Promocode.all #select from DB all promocodes
  end

#==============================Form for new code=================================

  def new
  	@title = "New Promo::Instaprinter" # title
  	add_breadcrumb "New promo", new_promocode_path, nil, 'abread'
  end

#===============================Creating new promo===============================

def create
	prom = Service.find(params[:service][:service_id]).promocodes.new(params[:promocode])
	prom.status = "FREE"
	prom.save
  	redirect_to promocodes_path
end

#===============================Deleting promo====================================

def destroy
	return unless request.xhr?
	Promocode.find(params[:id]).destroy
	respond_to do |format|
	  format.html {render :nothing => true}
	end
end

#============================Promo Validation=====================================

def validate
	return unless request.xhr?
	promo = Promocode.find_by_code(params[:promocode])
	@result = {}
	if !promo.nil? && session[:s] && promo.status == "FREE"
  	@result[:pid] = promo.id
		if promo.service_id.to_s == session[:s]
		  datetimenow = Time.now.to_formatted_s(:db)
		  validpromodate = promo.use_until.to_formatted_s(:db)
			if validpromodate > datetimenow			  
				@result[:res] = "0"
			else
				@result[:res] = "1" # already in use
			end
		else
			@result[:res] = "2" # it's ok, but not for specified service
		end
	else
		@result[:res] = "3" # code is invalid
	end
	respond_to do |format|
		format.html { render :layout => false }
	end
end

#================================================================================= 

end
