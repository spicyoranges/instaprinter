# -*- coding: utf-8 -*-
class AdminController < ApplicationController

#Login method
  def login
    @title = "Login::Adminka::Instaprinter" #setup title
    if params[:em]
      @em = "<div class='admerros'>#{params[:em]}</div>".html_safe #Error message
    end  	
  end

#Adminka index page
  def index
    if !session[:admin] #checking entrance
      redirect_to :action => "login" #if incorrect enter then redirect to login
    else
      @title = "Adminka::Instaprinter"
  	  @users = User.order("id")
  	  @orders = Order.order("id")
      session[:abread] = nil
      add_breadcrumb "Main page" , adminka_path, nil, 'abread'
    end
  end
  
  #authentication of admin
  def check
    if params[:login]
      admin = Admin.find_by_login(params[:login])
      if admin && admin.authenticate(params[:password]) 
        session[:admin] = admin.login
        redirect_to :action => "index"
      else
        redirect_to :action => 'login', :em => "Invalid login or password"
      end
    else
      redirect_to :action => 'login'
    end
  end

  def logout
    if !session[:admin]
      redirect_to adminka_path
    else
      session[:admin] = nil
      session[:abread] = nil
      redirect_to adminka_path
    end
  end

  def conditions
    if request.xhr?
      @start_date = Date.civil(params[:start]["post(1i)"].to_i,
                               params[:start]["post(2i)"].to_i,
                               params[:start]["post(3i)"].to_i)
      @end_date = Date.civil(params[:end]["post(1i)"].to_i,
                             params[:end]["post(2i)"].to_i,
                             params[:end]["post(3i)"].to_i)

      @conditions = Order.where(:date => @start_date..(@end_date+1.day)).order('date')

      if !params[:status].blank?
        @conditions = Order.where(:date => @start_date..(@end_date+1.day)).where("status = ?", params[:status]).order('date')
        if !params[:post]["user"].blank?
          uid = User.find_by_login(params[:post]["user"]).id
          @conditions = Order.where(:date => @start_date..(@end_date+1.day)).where("status = ?", params[:status]).where('user_id = ?', uid).order('date')
          if !params[:post]["stype"].blank?
            sid = Service.find_by_sname(params[:post]["stype"]).id
            @conditions = Order.where(:date => @start_date..(@end_date+1.day)).where("status = ?", params[:status]).where('user_id = ?', uid).where('service_id = ?', sid).order('date')
          end
        elsif !params[:post]["stype"].blank?
          sid = Service.find_by_sname(params[:post]["stype"]).id
          @conditions = Order.where(:date => @start_date..(@end_date+1.day)).where("status = ?", params[:status]).where('service_id = ?', sid).order('date')  
        end
      elsif !params[:post]["user"].blank?
        uid = User.find_by_login(params[:post]["user"]).id
        @conditions = Order.where(:date => @start_date..(@end_date+1.day)).where('user_id = ?', uid).order('date')
          if !params[:post]["stype"].blank?
            sid = Service.find_by_sname(params[:post]["stype"]).id
            @conditions = Order.where(:date => @start_date..(@end_date+1.day)).where('user_id = ?', uid).where('service_id = ?', sid).order('date')  
          end     
      elsif !params[:post]["stype"].blank?
        sid = Service.find_by_sname(params[:post]["stype"]).id
        @conditions = Order.where(:date => @start_date..(@end_date+1.day)).where('service_id = ?', sid).order('date')
      end
          
      respond_to do |format|
        format.js 
      end
    else
      redirect_to adminka_path
    end

  end

  def track
    if request.xhr?
      Order.update(params[:oid], :tracknum => params[:tracknum], :status => "Enviado")
      respond_to do |format|
        format.js
      end
    else
      redirect_to adminka_path
    end
  end

  def delete #mashenka!!!!!
    if params[:del]
      params[:del].each do |d|
        if !d.blank? 
          File.delete("#{Rails.root}/reports/#{Order.find(d).user.login}/#{Order.find(d).date.strftime('%Y%m%d')}_#{d}.pdf")
          Order.find(d).destroy
        end 
      end
    else
      redirect_to adminka_path
    end
  end

  def showadmins 
    if !session[:admin]
      redirect_to adminka_path
    else
      @title = "Admins::Instaprinter"
      @admins = Admin.all
      add_breadcrumb "Admins" , showadmins_path, nil, 'abread'
    end
  end

  def editadmin

    if !session[:admin]
      redirect_to adminka_path
    else
      if !params[:aid]
        @stat = "new"
        @login = ""
        @password = ""
        @submit = "create"
        add_breadcrumb "New admin" , editadmin_path, nil, 'abread'
        @title = "Create Admin::Instaprinter"
      else
        @stat = "edit"
        @login = Admin.find(params[:aid]).login
        @password = Admin.find(params[:aid]).password
        @submit = "Apply"
        add_breadcrumb @login , editadmin_path, nil, 'abread'
        @title = "#{@login}::Admins::Instaprinter"
      end 
    end
  end

  def createadmin
    if !params[:aid]
      redirect_to adminka_path
    else
      if params[:aid].blank?
        Admin.create(:login => params[:login], :password => params[:password])
      else
        Admin.update(params[:aid], :login => params[:login], :password => params[:password])
      end
      redirect_to showadmins_path
    end
  end
  
end