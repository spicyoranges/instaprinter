# -*- coding: utf-8 -*-
class ApplicationController < ActionController::Base
  protect_from_forgery
  require 'instagram'
  
protected 
 
  # Функция добавления бредкрамбсов  
  def add_breadcrumb name, url, pos, ses
  	
  	session[ses] ||= []
  	temp = []
  	i = 0
  	s = session[ses].size
  	
  	if !pos.nil?
  		while i < pos
  			temp[i] = session[ses][i]
  			i += 1
  		end
  		
  		session[ses] = temp
   		session[ses] << [name , url]  		
  	else
 		while i < s
  			temp[i] = session[ses][i]
  			break if temp[i][0] == name
  			i += 1
  		end
  	
  		session[ses] = temp
   		session[ses] << [name , url]  if i == s
  	
  	end
  end
  

  #Вспомогательная функция бредов
  def self.add_breadcrumb name, url, pos, options = {}

    before_filter options do |controller|
    controller.send(:add_breadcrumb, name, url)
    end

  end

  ActionController::Base.asset_host = Proc.new { |source|
  if source.starts_with?('/images')
    STATIC_HOST
  elsif source.starts_with?('/js')
    STATIC_HOST
  else
    STATIC_HOST
  end
}
  
      
end