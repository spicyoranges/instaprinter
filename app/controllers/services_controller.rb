# -*- coding: utf-8 -*-
class ServicesController < ApplicationController	
	
  def index # Не юзабелен (Надо вальнуть)

  	redirect_to root_path
  	
  end
  
  def show # Страница инфы о сервисе
  	
  	@title = "#{Service.find(params[:s]).sname.capitalize} :: InstaPrinter" # Заголовок страницы
  	@serv = Service.find(params[:s]) #Находим нужный сервис в базе
  	session[:s] = params[:s] #пердаем сервис в сессию
  	add_breadcrumb @serv.sname.capitalize , url_for(:controller => "services", :action => "show", :s => params[:s]), 1, 'bread' #Добавляем навигацию
  	
  end
  
end
