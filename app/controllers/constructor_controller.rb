# -*- coding: utf-8 -*-
class ConstructorController < ApplicationController
  def show
  	  	#Переадресация если нет ключа
  	redirect_to :controller => 'sessions', :action => 'connect' if !session[:access_token]
  	
  	add_breadcrumb "Constuctor", construct_path, nil #добавление навигации
  	@serv = Service.find(session[:s]) #достаем сервис из базы
    @user = session[:usr] #определяем переменную юзер
  end
end
