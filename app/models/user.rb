# -*- coding: utf-8 -*-
class User < ActiveRecord::Base
	has_many :orders
	has_many :promocodes
end
