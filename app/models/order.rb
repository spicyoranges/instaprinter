# -*- coding: utf-8 -*-
class Order < ActiveRecord::Base
	belongs_to :user
	belongs_to :service

  def self.ids
  	dirs = self.select(:direction).uniq
  	ids = []

    i = 0
    while i < dirs.size do
      ids << self.find_by_direction(dirs[i].direction, :limit => 1).direction
      i += 1
    end
    return ids
  end
end
