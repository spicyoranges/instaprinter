# -*- coding: utf-8 -*-
class Service < ActiveRecord::Base
	has_many :orders
	has_many :promocodes
end
