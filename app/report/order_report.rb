# encoding: utf-8
class OrderReport < Prawn::Document

  def to_pdf o_id, photos, color, vert

	#Берем айди сервиса из базы
	s = Order.find(o_id).service.id
		
	r = Random.new
	
	case s
	when 1 then fsize = 18
	when 2 then 
		if photos.size <= 6 
			fsize = 6
		else
			if photos.size.modulo(6) == 0
				fsize = photos.size
			else
				fsize = photos.size.div(6).next*6
			end
		end
	when 3 then 
        case photos.size
        when 1..50 then fsize = 50
        when 51..72 then fsize = 72
        when 73..98 then fsize = 98
        when 99..128 then fsize = 128
        when 129..162 then fsize = 162
        when 163..200 then fsize = 200
        when 201..242 then fsize = 242
        when 243..288 then fsize = 288
        when 288..338 then fsize = 338
        when 338..392 then fsize = 392
        else fsize = 392
        end
	else fsize = 0
	end
	
	
	i = 0
	k = 0
	
	photos_full = [] #Заполняем новый массив с учетом повтора фотографий
	while i < fsize.div(photos.size) do
		if (i == 0)or(s == 1) 
			photos.each do |ph|
				photos_full[k] = ph
				k += 1	
			end
		else
			photos.each do |ph|
				if photos.size > 1
					photos_full[k] = photos[r.rand(0...photos.size)]
				else 
					photos_full[k] = photos[0]
				end 
				k += 1	
			end			
		end
		i += 1
	end
	if fsize.modulo(photos.size) > 0 
		i =0
		while i < fsize.modulo(photos.size) do
			if s == 1 
				photos_full[k] = photos[i]
			else
				if photos.size > 1
					photos_full[k] = photos[r.rand(0...photos.size)]
				else 
					photos_full[k] = photos[0]
				end 
			end
			k += 1
			i += 1
		end
	end
	
	case s 

#==============================================================================

	when 1 then #Если это магниты 
		
        mar = 2.834645669		
		l = 0
        p = 0

        while p < 3 do
            i = 0
            start_new_page(:size => [486.226771654, 316.573228346], :margin => 0) 
    		px = (bounds.width - (141.732283465*3+28.346456693*2))/2
    		py = (bounds.height - (141.732283465*2+28.346456693))/2
    		y = bounds.top - py #Заполняем ПДФку фотками по рядам и столбцам
    		while i < 2 do
    			x = bounds.left + px
    			k = 0
    			while k < 3 do
    				image open(photos_full[l]), :width => 141.732283465, :at => [x,y]
    				l += 1
    				x += 141.732283465+28.346456693
    				k += 1
    			end
    			y -= 141.732283465+28.346456693
    			i += 1
    		end
            p += 1
        end

		# stroke do
		# 	stroke_color('000000')
		# 	y0 = bounds.top - py
		# 	x0 = bounds.left + px
		# 	3.times do
		# 		line(bounds.left, y0, bounds.left + 3, y0)
		# 		line(bounds.right - 3, y0, bounds.right, y0)
		# 		y0 -= 141
		# 	end
		# 	11.times do
		# 		line(x0, bounds.top, x0, bounds.top - 4)
		# 		line(x0, bounds.bottom, x0, bounds.bottom + 4)
		# 		x0 += 141
		# 	end
		# end

#=============================================================================
		
    when 3 then  #Если это постер

        #Selection of positioning of poster
        if vert == true
            start_new_page(:size => [1417, 2834], :margin => 0)
        else
            start_new_page(:size => [2834, 1417], :margin => 0)
        end
            
        #Filling poster with background color
        fill_color(color)
        fill_rectangle([bounds.left,bounds.top], bounds.width, bounds.height)
        
        #Mesearing calculations

        mar = 28
        w = bounds.width - 2*mar
        h = bounds.height - 2*mar
        s = fsize
        a = Math.sqrt((h*w)/s)
        pw = a/24
        ix = (w/a).round
        iy = (h/a).round
        pxdem = (ix+1)*pw
        ach = (w - pxdem)/ix 
        ph = (h - (ach*iy))/(iy + 1)
        i = 0
        k = 0
        l = 0
        py = (bounds.height - (iy*ach+(iy+1)*ph))/2 + ph
        px = (bounds.width - (ix*ach+(ix+1)*pw))/2 + pw

        #Filling poster with photos
        y = bounds.top - py     
        while i < iy do
            x = bounds.left + px
            k = 0
            while k < ix do
                image open(photos_full[l]), :width => ach, :at => [x,y]
                k += 1
                l += 1
                x += (ach + pw)
            end
            i += 1
            y -= (ach + ph)
        end

#=================================================================================
    	
    when 2 then  #Если это стикеры
    	
    	#Tapa 1
    	start_new_page(:size => [396, 241], :margin => 0)
    	stroke do
    		fill_color('FFFF88')
    		fill_rectangle([bounds.left, bounds.top], 57, bounds.height)
		end
    	   	 

    	#Pages with stickers   	
    	i = 0
    	n = 0
    	start_new_page(:size => [396, 241], :margin => 0)
    	px = ((bounds.width-71)-283)/2
    	py = (bounds.height-184)/2
    	
    	while i < fsize.div(6) do
    		stroke do
    			stroke_color = '#000000'
    			fill_color('FFFF88')
    			fill_rectangle([bounds.left, bounds.top], 57, bounds.height)   			
			end
    		y = bounds.top - py
    		x = bounds.left + 71 + px
    		k = 0
    		while k < 2 do
   			
    			image open(photos_full[n]), :width => 85, :at => [x,y]
    			image open(photos_full[n+1]), :width => 85, :at => [x+99,y]
    			image open(photos_full[n+2]), :width => 85, :at => [x+198,y]

    			n += 3
    			k += 1
    			y -= 99
    		end
    		i += 1
    		start_new_page if i < fsize.div(6)
    	end

    	#tapa 2
    	start_new_page
    	stroke do
    		fill_color('FFFF88')
    		fill_rectangle([bounds.left, bounds.top], 57, bounds.height)
		end
    
    	#Instructions for printmasters
    	start_new_page :margin => 20
    	fill_color('000000')
    	text "Instrucciónes para imprimir", :style => :bold, :align => :center, :size => 16

    	if 20.modulo(fsize.div(6)) == 0
    		pgns ='2'
    		i = 0
    		while i < fsize.div(6)-1 do
    			pgns = pgns + ", #{i+3}"
    			i += 1
    		end
    		text "Pagina(s): #{pgns} por #{20.div(fsize.div(6))} copies"
    	else
    		pgns = '2'
    		i = 0
    		while i < 20.modulo(fsize.div(6))-1 do
    			pgns = pgns + ", #{i+3}"
    			i += 1
    		end
    		pgnsmas = "#{i+3}"
    		i += 1
    		while i < fsize.div(6)-1
    			pgnsmas = pgnsmas + ", #{i+3}"
    			i += 1
    		end
    		text "Pagina(s): #{pgns} son #{20.div(fsize.div(6))+1} copias y #{pgnsmas} son #{20.div(fsize.div(6))} copias"
    	end
    	
    	text "Cantidad de paginas: 20 + 2 tapas"

#======================================================================================
    		
    else text "No hay producto con este numero !"
	end

    render_file Rails.root.join("reports/#{Order.find(o_id).user.login}", "#{Order.find(o_id).date.strftime('%Y%m%d')}_#{o_id}.pdf")
    Order.update(o_id, :status => "Generado", 
    :pdfref => "https://pdf.instaprinter.cl/#{Order.find(o_id).user.login}/#{Order.find(o_id).date.strftime('%Y%m%d')}_#{o_id}.pdf")
  end
  
end