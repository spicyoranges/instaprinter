# -*- coding: utf-8 -*-
class UserMailer < ActionMailer::Base
  default from: "Instaprinter Chile"
  
  def confirmail(o,user)
    @order = Billing.find(o).id
  	@user = User.find_by_login(user)
  	mail(:to => @user.email, :subject => 'Instaprinter - aviso de compra')
  end
end